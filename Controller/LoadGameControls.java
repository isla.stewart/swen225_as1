package Controller;

import Model.Game;

import Model.Serializer;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
/**
 * The Controller for  the Load Game View. Sets up a game from a valid Load String
 * and heads to the Main View. If the String is invalid, then the user is sent back
 * to the Welcome View. 
 */
public class LoadGameControls extends AbstractControls{

    public LoadGameControls(Controller controller){
        super(controller);
    }

    class SubmitLoadGameListener implements ActionListener{
        public void actionPerformed(ActionEvent actionEvent) {
            String loadString = theView.getLoadGame().getLoadString();
            File loadFile = theView.getLoadGame().getLoadFile();
            if(loadFile != null){
                Serializer s = new Serializer();
                if(s.loadFromFile(loadFile)){
                    theController.setTheModel(Game.loadGame(s));
                    theController.viewMain();
                } else {
                    JOptionPane.showMessageDialog(null, "Invalid load file",
                            "The file submitted was invalid. ", JOptionPane.INFORMATION_MESSAGE);
                    theController.viewWelcome();
                }
            } else if(loadString.length() > 0){
                Serializer s = new Serializer();
                if(s.generateGame(loadString)){
                    theController.setTheModel(Game.loadGame(s));
                    theController.viewMain();
                } else {
                    JOptionPane.showMessageDialog(null, "Invalid load string",
                            "The save string submitted was invalid. ", JOptionPane.INFORMATION_MESSAGE);
                    theController.viewWelcome();
                }

            } else {
                JOptionPane.showMessageDialog(null, "Please load a file or enter a game string",
                        "Nothing to load: ", JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }

    public ActionListener getSubmitLoadGameListener() { return new SubmitLoadGameListener(); }

    class BackToMenuListener implements ActionListener {
        public void actionPerformed(ActionEvent actionEvent) {
            theController.viewWelcome();
        }
    }

    public ActionListener getBackToMenuListener(){
        return new BackToMenuListener();
    }
}
