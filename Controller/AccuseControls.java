package Controller;

import Model.Game;
import View.GUI;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * The Controller for 'Creating an Accusation' View. 
 * This either goes to the Result View if the Accusation is correct or if after the accusation,
 * all the Players are dead. 
 * Otherwise, it goes back to the Main View. 
 */
public class AccuseControls {

    private GUI theView;
    private Game theModel;
    private Controller theController;

    public AccuseControls(Controller controller){
        super();
        theController = controller;
        theView = theController.getTheView();
        theModel = theController.getTheModel();
    }

    class AccuseSubmitListener implements ActionListener {
        public void actionPerformed(ActionEvent actionEvent) {
            if(theView.getAccuse().getAccusation() != null){
                if(theView.getAccuse().getAccusation().compareTo(theModel.getSolution())){
                    theController.viewResult();
                } else {
                    theModel.getCurrentPlayer().killPlayer();
                    JOptionPane.showMessageDialog(null,
                            "You got it wrong! You're dead"
                    );
                    if(theModel.checkAllDead()){
                        theController.viewResult();
                    } else {

                        theController.viewMain();

                    }
                }
            }
        }
    }

    public ActionListener getAccuseSubmitListener(){
        return new AccuseSubmitListener();
    }
}
