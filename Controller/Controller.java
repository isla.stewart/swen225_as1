package Controller;

import Model.*;
import View.*;

import javax.swing.*;

/**
 * This class allows the game to move through from one View of the game
 * to another through their Controls. 
 *
 */
public class Controller {

    private GUI theView;
    private Game theModel;

    private boolean gameSaved;

    public Controller(GUI view, Game model){
        theView = view;
        theModel = model;
        gameSaved = true;
        MenuControls menuControls = new MenuControls(this);
        theView.addSaveGameListener(menuControls.getSaveGameListener());
        theView.addNewGameListener(menuControls.getNewGameListener());
        theView.addLoadGameListener(menuControls.getLoadGameListener());
        theView.addQuitToMenuListener(menuControls.getQuitToMenuListener());
        theView.addQuitGameListener(menuControls.getQuitGameListener());
        theView.addExitWindowListener(menuControls.getExitWindowListener());
        viewWelcome();
    }

    public GUI getTheView() {
        return theView;
    }

    public Game getTheModel() {
        return theModel;
    }

    public boolean isGameNotSaved() { return !gameSaved; }

    public void setTheView(GUI theView) { this.theView = theView; }

    public void setTheModel(Game theModel) { this.theModel = theModel; }

    public void setGameSaved(boolean gameSaved) { this.gameSaved = gameSaved; }



    public void viewAccuse(){
        // Create the screen
        theModel.setMovesRemaining(0);
        Accuse accuse = new Accuse(theView, theModel);
        AccuseControls accuseControls = new AccuseControls(this);
        // Edit the screen
        accuse.addSubmitListener(accuseControls.getAccuseSubmitListener());

        // Show the screen
        theView.setAccuse(accuse);
        theView.show(accuse);
    }

    public void viewLoadGame(){
        // Create the screen
        LoadGame loadGame = new LoadGame(theView, theModel);
        LoadGameControls loadGameControls = new LoadGameControls(this);
        // Edit the screen
        loadGame.addSubmitListener(loadGameControls.getSubmitLoadGameListener());
        loadGame.addBackListener(loadGameControls.getBackToMenuListener());
        // Show the screen
        theView.setLoadGame(loadGame);
        theView.show(loadGame);

    }

    public void viewMain(){
        // Create the screen
        Main main = new Main(theView, theModel);
        MainControls mainControls = new MainControls(this);

        // Set the appropriate turn names
        mainControls.updateGameHeader(main);

        // Make the top buttons work.
        main.accuse.addActionListener(mainControls.getAccuseListener());

        main.suggest.addActionListener(mainControls.getSuggestListener());

        main.endTurn.addActionListener(mainControls.getEndTurnListener());

        // Add mouse listener to the dice panel.
        main.dicePanel.addMouseListener(mainControls.getDiceRollListener());

        if(theView.getKeyListeners().length == 0) { // Ensure it isn't duplicated.
            // add key listeners
            theView.setFocusable(true);
            theView.addKeyListener(mainControls.getArrowListener());
        }

        // Show the screen
        theView.setMain(main);
        theView.show(main);

        gameSaved = false;

        if(theModel.getGamePhase() == Game.GamePhase.AWAIT_DICE) {
            // Enable saving for the user
            theView.enableSaveGame(true);
        }

        if(theModel.getCardRefuted() != null) {
            Card c = theModel.getCardRefuted();
            JOptionPane.showMessageDialog(null,
                    "Your suggestion has been refuted. Someone has the " + c.getDescription() + " card!"
            );
            theModel.setCardRefuted(null);
        }
    }

    public void viewNewGame(){
        // Create the screen
        NewGame newGame = new NewGame(theView, theModel);
        NewGameControls newGameControls = new NewGameControls(this);
        // Edit the screen
        newGame.addSubmitListener(newGameControls.getSubmitNewGameListener());
        newGame.addBackListener(newGameControls.getBackToMenuListener());
        // Show the screen
        theView.setNewGame(newGame);
        theView.show(newGame);
    }

    public void viewRefute(){
        // Create the screen
        Refute refute = new Refute(theView, theModel);
        RefuteControls refuteControls = new RefuteControls(this);
        // Edit the screen
        refute.addSubmitListener(refuteControls.getRefuteSubmitListener());
        // Show the screen
        theView.setRefute(refute);
        theView.show(refute);
    }

    public void viewResult(){
        gameSaved = true;
        // Create the screen
        Result result = new Result(theView, theModel);
        ResultControls resultControls = new ResultControls(this);
        // Edit the screen
        result.addMainMenuListener(resultControls.getMainMenuListener());
        // Show the screen
        theView.setResult(result);
        theView.show(result);
    }

    public void viewSuggest(){
        // Create the screen
        theModel.setMovesRemaining(0);
        Suggest suggest = new Suggest(theView, theModel);
        SuggestControls suggestControls = new SuggestControls(this);
        // Edit the screen
        suggest.addSubmitListener(suggestControls.getSuggestSubmitListener());
        // Show the screen
        theView.setSuggest(suggest);
        theView.show(suggest);
    }

    public void viewWelcome(){
        gameSaved = true;
        // Create the screen
        Welcome welcome = new Welcome(theView, theModel);
        WelcomeControls welcomeControls = new WelcomeControls(this);
        // Edit the screen
        welcome.addNewGameListener(welcomeControls.getNewGameListener());
        welcome.addLoadGameListener(welcomeControls.getLoadGameListener());
        // Show the screen
        theView.setWelcome(welcome);
        theView.show(welcome);
    }

}