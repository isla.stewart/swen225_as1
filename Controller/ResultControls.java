package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * This Controls the View shown at the conclusion of the game. 
 * It presents a different messege based on if there is a winner.
 * The user can go back to the Welcome View from here.
 *
 */
public class ResultControls extends AbstractControls{

	public ResultControls(Controller controller){
        super(controller);
    }

    class mainMenuListener implements ActionListener {
        public void actionPerformed(ActionEvent ae){
            theController.viewWelcome();
        }
    }

    public ActionListener getMainMenuListener() { return new mainMenuListener(); }

}
