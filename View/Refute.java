package View;

import Model.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Refute extends AbstractPanel {

    public Refute(GUI view, Game model){
        super(view, model);
    }

    private JButton submit;
    private Card selectedCard;

    @Override
    protected void editTopHeader() {
        JLabel title = new JLabel(theModel.getCurrentRefuter().getName() + " can refute.");
        title.setBorder(new EmptyBorder(10,0,0,0));
        topHeader.add(title);
    }

    @Override
    protected void editBottomHeader() {
        submit = new JButton("Refute the suggestion");
        submit.setEnabled(false);
        bottomHeader.add(submit);
    }

    @Override
    protected void editBody() {
        drawing = new DrawingBoard() {
            protected void paintComponent(Graphics g) {
                Hand h = theModel.getCurrentRefuter().getHand();
                h.draw(g, drawing.getWidth(), drawing.getHeight(), theView.getRefute().getSelectedCard(), theModel.getCurrentSuggestion());
            }
        };

        /* Add a new mouse adaptor to the to the window to detect chosen cards. */
        drawing.addMouseListener(new MouseAdapter() {
            public void mouseReleased(MouseEvent e) { // Updates every time the board is clicked on
                Hand h = theModel.getCurrentRefuter().getHand();
                int cardNum;
                if((drawing.getWidth() * 0.025) < e.getX() && e.getX() < (drawing.getWidth() * 0.325)){ // First Column
                    cardNum = 1;
                } else if((drawing.getWidth() * 0.35) < e.getX() && e.getX() < (drawing.getWidth() * 0.65)){ // Second Column
                    cardNum = 2;
                } else if((drawing.getWidth() * 0.675) < e.getX() && e.getX() < (drawing.getWidth() * 0.975)){ // Third Column
                    cardNum = 3;
                } else {
                    // Then Click was not on a card
                    cardNum = 0;
                }

                if((drawing.getHeight() * 0.025) < e.getY() && e.getY() < (drawing.getHeight() * 0.475)){
                    // Then Click was on top row of cards
                } else if((drawing.getHeight() * 0.525) < e.getY() && e.getY() < (drawing.getHeight() * 0.975)){
                    // Then Click was on 2nd row of cards
                    cardNum += 3;
                } else {
                    // Then Click was not on a card
                    cardNum = 0;
                }
                if(cardNum > 0){
                    Card c = h.getCard(cardNum - 1);
                    if(theModel.getCurrentSuggestion().containsCard(c)){
                        theView.getRefute().setSelectedCard(c);
                        theView.getRefute().enableSubmit(true);
                    }
                }

                drawing.repaint();
            }
        });
        body.add(drawing);
    }

    @Override
    protected void editFooter() {

    }

    public Card getSelectedCard(){
        return selectedCard;
    }

    public void setSelectedCard(Card c){
        selectedCard = c;
    }

    /**
     * Add action listener to the submit button
     */
    public void addSubmitListener(ActionListener aL){
        submit.addActionListener(aL);
    }

    /**
     * Change submit button availablility
     */
    public void enableSubmit(boolean enabled){
        submit.setEnabled(enabled);
    }
}
