package View;

import Model.Game;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class NewGame extends AbstractPanel {

    private JButton submit;
    private JButton back;

    private int numOfPlayers;
    private boolean scarlettChosen;
    private boolean mustardChosen;
    private boolean whiteChosen;
    private boolean greenChosen;
    private boolean peacockChosen;
    private boolean plumChosen;

    private PlayerInfo playerOne;
    private PlayerInfo playerTwo;
    private PlayerInfo playerThree;
    private PlayerInfo playerFour;
    private PlayerInfo playerFive;
    private PlayerInfo playerSix;

    public NewGame(GUI view, Game model){
        super(view, model);
    }

    /**
     * One of these classes will be created for each available character.
     */
    private class PlayerInfo extends JPanel{
        public int characterNum = 0;
        public int playerNum;
        public boolean enabled;
        private JTextField name = new JTextField();
        private ButtonGroup characterSelection = new ButtonGroup();
        private JRadioButton scarlett = new JRadioButton("Miss Scarlett");
        private JRadioButton mustard = new JRadioButton("Colonel Mustard");
        private JRadioButton white = new JRadioButton("Mrs. White");
        private JRadioButton green = new JRadioButton("Mr. Green");
        private JRadioButton peacock = new JRadioButton("Mrs. Peacock");
        private JRadioButton plum = new JRadioButton("Professor Plum");
        private ActionListener characterSelected = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

                characterNum = Integer.parseInt(actionEvent.getActionCommand());

                checkChosen();

                playerOne.checkEnabled();
                playerTwo.checkEnabled();
                playerThree.checkEnabled();
                playerFour.checkEnabled();
                playerFive.checkEnabled();
                playerSix.checkEnabled();

                checkChosen();
            }
        };

        public PlayerInfo(int num){
            super();
            this.playerNum = num;
            this.enabled = numOfPlayers >= playerNum;
            initialise();
            checkEnabled();
        }

        /**
         * Set up the Swing objects for the field.
         */
        private void initialise(){
            this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

            JPanel playerName = new JPanel();
            playerName.setLayout(new BoxLayout(playerName, BoxLayout.LINE_AXIS));
            name.setToolTipText("3 - 10 characters");
            name.setMaximumSize(new Dimension((int) (GUI.DEFAULT_FRAME_WIDTH * 0.2), (int) (GUI.DEFAULT_FRAME_HEIGHT * 0.05)));
            JLabel nameLabel = new JLabel("Player Name: ");
            playerName.add(nameLabel);
            playerName.add(name);

            scarlett.setMnemonic(1);
            scarlett.setActionCommand("1");

            mustard.setMnemonic(2);
            mustard.setActionCommand("2");

            white.setMnemonic(3);
            white.setActionCommand("3");

            green.setMnemonic(4);
            green.setActionCommand("4");

            peacock.setMnemonic(5);
            peacock.setActionCommand("5");

            plum.setMnemonic(6);
            plum.setActionCommand("6");

            characterSelection = new ButtonGroup();
            characterSelection.add(scarlett);
            characterSelection.add(mustard);
            characterSelection.add(white);
            characterSelection.add(green);
            characterSelection.add(peacock);
            characterSelection.add(plum);

            scarlett.addActionListener(characterSelected);
            mustard.addActionListener(characterSelected);
            white.addActionListener(characterSelected);
            green.addActionListener(characterSelected);
            peacock.addActionListener(characterSelected);
            plum.addActionListener(characterSelected);

            JLabel label1 = new JLabel("Select Character:");

            JPanel characters = new JPanel(new GridLayout(0,1));
            characters.add(label1);
            characters.add(scarlett);
            characters.add(mustard);
            characters.add(white);
            characters.add(green);
            characters.add(peacock);
            characters.add(plum);

            JLabel playerNumber = new JLabel("Player " + playerNum);
            this.add(playerNumber);
            this.add(playerName);
            this.add(characters);
        }

        public String getName() {
            return name.getText();
        }

        public int getCharacterNum() {
            return characterNum;
        }

        /**
         * Return if the name is filled out.
         * @return true if it has.
         */
        public boolean isFilledOut() {
            return characterNum > 0 && name.getText().length() >= 3 && name.getText().length() <= 10;
        }

        /**
         * Check the number of players that a user has chosen and switch the available fields
         */
        public void checkEnabled() {
            enabled = numOfPlayers >= playerNum;
            this.name.setEnabled(enabled);
            if(!enabled){
                characterSelection.clearSelection();
            }
            availableCharacters();
        }

        /**
         * Get available characters and set the enabling on buttons
         */
        public void availableCharacters(){
            this.scarlett.setEnabled(enabled && !scarlettChosen);
            this.mustard.setEnabled(enabled && !mustardChosen);
            this.white.setEnabled(enabled && !whiteChosen);
            this.green.setEnabled(enabled && !greenChosen);
            this.peacock.setEnabled(enabled && !peacockChosen);
            this.plum.setEnabled(enabled && !plumChosen);
        }
    }


    /**
     * Ensure that selected characters are disabled for the other playesr.
     */
    public void checkChosen(){
        scarlettChosen = playerOne.scarlett.isSelected() || playerTwo.scarlett.isSelected()
                || playerThree.scarlett.isSelected() || playerFour.scarlett.isSelected()
                || playerFive.scarlett.isSelected() || playerSix.scarlett.isSelected();
        mustardChosen = playerOne.mustard.isSelected() || playerTwo.mustard.isSelected()
                || playerThree.mustard.isSelected() || playerFour.mustard.isSelected()
                || playerFive.mustard.isSelected() || playerSix.mustard.isSelected();
        whiteChosen = playerOne.white.isSelected() || playerTwo.white.isSelected()
                || playerThree.white.isSelected() || playerFour.white.isSelected()
                || playerFive.white.isSelected() || playerSix.white.isSelected();
        greenChosen = playerOne.green.isSelected() || playerTwo.green.isSelected()
                || playerThree.green.isSelected() || playerFour.green.isSelected()
                || playerFive.green.isSelected() || playerSix.green.isSelected();
        peacockChosen = playerOne.peacock.isSelected() || playerTwo.peacock.isSelected()
                || playerThree.peacock.isSelected() || playerFour.peacock.isSelected()
                || playerFive.peacock.isSelected() || playerSix.peacock.isSelected();
        plumChosen = playerOne.plum.isSelected() || playerTwo.plum.isSelected()
                || playerThree.plum.isSelected() || playerFour.plum.isSelected()
                || playerFive.plum.isSelected() || playerSix.plum.isSelected();
        playerOne.availableCharacters();
        playerTwo.availableCharacters();
        playerThree.availableCharacters();
        playerFour.availableCharacters();
        playerFive.availableCharacters();
        playerSix.availableCharacters();
    }


    @Override
    protected void editTopHeader() {
        JLabel title = new JLabel("<html><b><font size = 20>New Game</b></html>");
        title.setBorder(new EmptyBorder(10,0,0,0));
        topHeader.add(title);

    }

    @Override
    protected void editBottomHeader() {
        JLabel subtitle = new JLabel("<html><i><font size = 5>Enter Player Details</i></html>");
        submit = new JButton("Submit");

        bottomHeader.add(subtitle);
        bottomHeader.add(submit);

    }

    @Override
    protected void editBody() {
        numOfPlayers = 3;
        scarlettChosen = false;
        mustardChosen = false;
        whiteChosen = false;
        greenChosen = false;
        peacockChosen = false;
        plumChosen = false;

        playerOne = new PlayerInfo(1);
        playerTwo = new PlayerInfo(2);
        playerThree = new PlayerInfo(3);
        playerFour = new PlayerInfo(4);
        playerFive = new PlayerInfo(5);
        playerSix = new PlayerInfo(6);

        // Player Info
        JPanel playerInformation = new JPanel( new GridLayout(2,3));

        playerInformation.add(playerOne);
        playerInformation.add(playerTwo);
        playerInformation.add(playerThree);
        playerInformation.add(playerFour);
        playerInformation.add(playerFive);
        playerInformation.add(playerSix);

        // Select number of players

        JRadioButton threePlayers = new JRadioButton("3");
        threePlayers.setMnemonic(3);
        threePlayers.setActionCommand("3");
        threePlayers.setSelected(true);

        JRadioButton fourPlayers = new JRadioButton("4");
        fourPlayers.setMnemonic(4);
        fourPlayers.setActionCommand("4");

        JRadioButton fivePlayers = new JRadioButton("5");
        fivePlayers.setMnemonic(5);
        fivePlayers.setActionCommand("5");

        JRadioButton sixPlayers = new JRadioButton("6");
        sixPlayers.setMnemonic(6);
        sixPlayers.setActionCommand("6");

        ButtonGroup selectNumOfPlayers = new ButtonGroup();
        selectNumOfPlayers.add(threePlayers);
        selectNumOfPlayers.add(fourPlayers);
        selectNumOfPlayers.add(fivePlayers);
        selectNumOfPlayers.add(sixPlayers);

        ActionListener setNumOfPlayers = actionEvent -> {
            numOfPlayers = Integer.parseInt(actionEvent.getActionCommand());
            playerFour.checkEnabled();
            playerFive.checkEnabled();
            playerSix.checkEnabled();
            checkChosen();
        };
        threePlayers.addActionListener(setNumOfPlayers);
        fourPlayers.addActionListener(setNumOfPlayers);
        fivePlayers.addActionListener(setNumOfPlayers);
        sixPlayers.addActionListener(setNumOfPlayers);

        JLabel label2 = new JLabel("Select number of players: ");

        JPanel radioPanel = new JPanel(new GridLayout(1, 0));
        radioPanel.add(label2);
        radioPanel.add(threePlayers);
        radioPanel.add(fourPlayers);
        radioPanel.add(fivePlayers);
        radioPanel.add(sixPlayers);

        JScrollPane scroll = new JScrollPane(playerInformation, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        body.setLayout(new BoxLayout(body, BoxLayout.PAGE_AXIS));
        body.add(radioPanel);
        body.add(scroll);
    }


    @Override
    protected void editFooter() {
        back = new JButton("Back to menu");
        footer.add(back);
    }


    /**
     * Add the submit action listner to the right object
     */
    public void addSubmitListener(ActionListener aL){
        submit.addActionListener(aL);
    }

    /**
     * Add the back action listener to the right swing object.
     */
    public void addBackListener(ActionListener aL){ back.addActionListener(aL); }

    /**
     * Are the fields all completed?
     */
    public boolean detailsFilledOut(){
        return playerOne.isFilledOut() && playerTwo.isFilledOut() && playerThree.isFilledOut()
                && (playerFour.isFilledOut() || numOfPlayers < 4)
                && (playerFive.isFilledOut() || numOfPlayers < 5)
                && (playerSix.isFilledOut() || numOfPlayers < 6);
    }

    public Integer[] getCharacters(){
        return new Integer[] {playerOne.getCharacterNum(), playerTwo.getCharacterNum()
                , playerThree.getCharacterNum(), playerFour.getCharacterNum()
                , playerFive.getCharacterNum(), playerSix.getCharacterNum()};
    }

    public String[] getNames(){
        return new String[] {playerOne.getName(), playerTwo.getName(), playerThree.getName()
                , playerFour.getName(), playerFive.getName(), playerSix.getName()};
    }
}
