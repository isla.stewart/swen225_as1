package Model;/*PLEASE DO NOT EDIT THIS CODE*/
/*This code was generated using the UMPLE 1.30.0.5092.1e2e91fc6 modeling language!*/

/*I DO  WHAT I WANT!*/

import java.awt.*;
import java.util.*;
import java.util.List;

// line 29 "model.ump"
// line 130 "model.ump"
/**
 * This Class is the Board for the Cluedo game.
 *
 * It contains all the cells on the Board, and when Player-Characters move,
 * they do so through this Class.
 *
 * This Class interacts with the Player to determine their movement on the Board
 *
 * @author amer
 *
 */
public class Board
{

	public enum Direction {
		UP,
		DOWN,
		LEFT,
		RIGHT,
		//The below directions are exclusively used for Room Jumping (Diagonals)
		LEFTUP,
		LEFTDOWN,
		RIGHTUP,
		RIGHTDOWN,

	}

	//------------------------
	// MEMBER VARIABLES
	//------------------------

	//Board Associations
	private List<Cell> cells;

	//Board Size specifactions
	public static final int ROWS = 25;
	public static final int COLUMNS = 24;

	/**Character Location Map*/
	//Stores the current location of all Characters on the Board
	Map <Card, Cell> characterCells = new HashMap<>();
	//Stores the Starting Point of all the Characters
	Map<Card, Cell> characterStartCells = new HashMap<>();

	//------------------------
	// CONSTRUCTOR
	//------------------------

	public Board()
	{
		cells = new ArrayList<>();
	}

	//------------------------
	// INTERFACE
	//------------------------
	/* Code from template association_GetMany */
	public Cell getCell(int index)
	{
		return cells.get(index);
	}

	/**
	 * We want this to be a 2D array.
	 */
	public List<Cell> getCells()
	{
		return Collections.unmodifiableList(cells);
	}

	public int numberOfCells()
	{
		return cells.size();
	}

	/* Code from template association_AddUnidirectionalMany */
	public boolean addCell(Cell aCell)
	{
		if (cells.contains(aCell)) { return false; }
		cells.add(aCell);
		return true;
	}

	/* Code from template association_AddIndexControlFunctions */
	public void addCellAt(Cell aCell, int index)
	{
		if(addCell(aCell))
		{
			if(index < 0 ) { index = 0; }
			if(index > numberOfCells()) { index = numberOfCells() - 1; }
			cells.remove(aCell);
			cells.add(index, aCell);
		}
	}

	public Map <Direction, Cell> getValidMoves(Player player, Map<Direction, Cell> moves) {
		Map <Direction, Cell> validMoves = new HashMap<>();
		for(Direction d : moves.keySet()) {
			Cell c = moves.get(d);
			//if c has been travelled to, or is not valid, the player cannot move to it
			if(!player.lastTravelledCells.contains(c) && isValidMove(player.getCurrentCell(), c)) {
				validMoves.put(d, c);
			}
		}
		return validMoves;
	}

	public boolean hasNoValidMoves(Player player){
		Cell currentCell = player.getCurrentCell();

		//Get the neighbouring Cells of the current Cell
		Map <Direction, Cell> moves = neighbourCells(currentCell);

		//check which of the possible moves are valid
		Map <Direction, Cell> validMoves = getValidMoves(player, moves);

		return validMoves.isEmpty();
	}

	/**
	 * Initiates the process of moving a Player-Character by one Cell.
	 *
	 * This will try to move to the given cell, will return a boolean indicating the success of
	 * the action.
	 *
	 * @param player --- The Player moving their Character.
	 * @param moveTo -- Cell the player is going to move to.
	 * @return the initial Cell of the Player-Character was on
	 */
	// line 35 "model.ump"
	public boolean move(Player player, Cell moveTo){
		// TODO: Ensure we boundary check for valid moves.
		//extract the current cell the player-character is on
		Cell currentCell = player.getCurrentCell();

		//Put the currentCell in the travelled cell list so we don't retread travelled ground
		player.lastTravelledCells.add(currentCell);

		//Get the neighbouring Cells of the current Cell
		Map <Direction, Cell> moves = neighbourCells(currentCell);

		//check which of the possible moves are valid
		Map <Direction, Cell> validMoves = getValidMoves(player, moves);

		if(validMoves.containsValue(moveTo)) {
			// Looks like this move is valid. Time to move the piece.
			try {
				//new Cell obtained, move the Player to the new Cell
				if(!player.move(moveTo)) //throws an Exception if the move is unsuccessful
					throw new InvalidMoveException("Failed attempt at basic move.");
			} catch (NullPointerException | InvalidMoveException e) {
				if(e instanceof NullPointerException)
					e.printStackTrace();
			}
			//Move the player-Character on the Map - Update the Map
			characterCells.put(player.getCharacter(), moveTo);

			return true;
		} else {
			return false;
		}
	}

	/**
	 * Checks all the conditions necessary for a Player-Character to move from
	 * the original Cell to the moveTo Cell
	 * @param original original cell
	 * @param moveTo cell to move to
	 * @return true if all conditions are met, otherwise false.
	 */
	public boolean isValidMove(Cell original, Cell moveTo){
		//if the Cell is occupied by a non-NPC Player-Character it cannot be moved to
		if(moveTo.hasRealPlayer()) return false;
		//if the cell is an entry, the player can move to an entry cell of a different room
		if(moveTo.getIsEntrance() && original.getIsEntrance()) return true;
		//in all cases, the player can move to a cell which is a part of the same room
		return original.getRoom() == moveTo.getRoom();
	}

	/**
	 * Jumps a Player-Character from their original cell to a valid Cell in the same Room
	 * as the Room of the given cell. Mainly used for when a Character is called to a
	 * Room when a Suggestion is made.
	 *
	 * @param player player to move
	 */
	public void jumpCell(Player player, Cell cell) {
		//get all the Cells, the Player-Character could possibly jump to
		Map<Direction, Cell> jump = neighbourCells(cell);
		//This is a special case, so the diagonal cells must be considered as well
		//add the diagonals to the Map
		int cellRow = cell.getRow();
		int cellCol = cell.getCol();
		if(getCell(cellRow - 1, cellCol - 1) != null)
			jump.put(Direction.LEFTUP, getCell(cellRow - 1, cellCol - 1));
		if(getCell(cellRow + 1, cellCol - 1) != null)
			jump.put(Direction.LEFTDOWN, getCell(cellRow + 1, cellCol - 1));
		if(getCell(cellRow - 1, cellCol + 1) != null)
			jump.put(Direction.RIGHTUP, getCell(cellRow - 1, cellCol + 1));
		if(getCell(cellRow + 1, cellCol + 1) != null)
			jump.put(Direction.RIGHTDOWN, getCell(cellRow + 1, cellCol + 1));
		Map<Direction, Cell> validJump = new HashMap<>();
		//check which Cells are valid/can be moved to
		for(Direction d : jump.keySet()) {
			Cell c = jump.get(d);
			/*In this scenario,
			 * the condition: 'valid because of being entry cells in different rooms'
			 * is not applicable. Therefore, that condition must be excluded from
			 * the valid cells. Only 'The same Room' condition is valid.
			 */
			if(cell.getRoom() == c.getRoom())
				validJump.put(d, c);
		}

		Cell original = player.getCurrentCell(); //to return
		//Now, the Player-Character can be placed in any Cell free

		for(Cell c : validJump.values()) {
			if(!c.hasRealPlayer()) {
				//Cell is free, move here
				try {
					if(!player.move(c)) //throws an Exception if the move is unsuccessful
						throw new InvalidMoveException("Failed attempt at basic move.");
				} catch (NullPointerException | InvalidMoveException e) {
					if(e instanceof NullPointerException)
						e.printStackTrace();
				}

				//Move the player-Character on the Map - Update the Map
				characterCells.put(player.getCharacter(), c);
				break;
			}
		}

	}

	/**
	 * Moves a player back to their starting Cell, if that Character's starting Cell is occupied by
	 * another non-NPC, we check the next possible free starting Cell
	 * @param player player to reset
	 * @return a Map of the Neighbouring Cells which can be travelled to from the reset Cell
	 */
	public Map<Direction, Cell> resetPlayer(Player player) {
		// TODO implement reset player
		//Possible moves from the reset position
		Map <Direction, Cell> moves = new HashMap<>();
		//found reset position flag
		boolean hasReset = false;
		Cell moveTo = characterStartCells.get(player.getCharacter());
		//if a starting Cell has been considered and rejected, it is placed in this list to avoid
		//being checked
		List<Cell> checkedStartPoints = new ArrayList<>();
		boolean originalPointFail = false;

		while(!hasReset) {
			//check if the Player-Character's starting Cell is occupied by a non-NPC or as already been checked
			if(originalPointFail) {
				for(Cell c : characterStartCells.values()) {
					if(!c.hasRealPlayer() && !checkedStartPoints.contains(c)) {
						//This cell is free
						moveTo = c;
						break;
					}
				}
			}
			/*
			 * ASSURANCE CHECK: if the player is trapped at their starting Cell,
			 * they are moved to a different starting Cell.
			*/
			Map <Direction, Cell> neigh = neighbourCells(moveTo);
			for(Direction d : neigh.keySet()) {
				//If there is even one valid move, then the Player can safely reset
				if(isValidMove(moveTo, neigh.get(d))) {
					//Exit the while loop at the end of the outer iteration
					hasReset = true;
					moves.put(d, neigh.get(d));
					break;
				}
			}
			/*all iterations of the above loop occur if there
			 * is nowhere for the Player to go from the considered Cell
			*/
			//The Cell is added to the checked List for the next iteration
			checkedStartPoints.add(moveTo);
			//If the first iteration reaches here, the original cell cannot
			//be moved to, so find another cell
			originalPointFail = true;
		}
		try {
			if(!player.move(moveTo)) //throws an Exception if the move is unsuccessful
				throw new InvalidMoveException("Failed attempt at basic move.");
		} catch (NullPointerException | InvalidMoveException e) {
			if(e instanceof NullPointerException)
				e.printStackTrace();
		}
		//Move the player-Character on the Map - Update the Map
		characterCells.put(player.getCharacter(), moveTo);
		return moves;
	}

	/**
	 * Returns the Cell at a given row and column on the Board. Returns null if ArrayIndexOutOfBounds
	 * @param row cell row
	 * @param col cell col
	 * @return the Cell at the given row and col
	 */
	// line 39 "model.ump"
	public Cell getCell(int row, int col){
		if(row < 0 || row >= ROWS || col < 0 || col >=COLUMNS)
			return null;
		return cells.get((row*COLUMNS) + col);
	}

	/**
	 * Output pretty ASCII display of board
	 */
	// line 42 "model.ump"
	public String toString(){
		String boardString = "";
		StringBuilder sb = new StringBuilder(boardString);
		int j = 0;
		for (Cell cell : cells) {
			if (j == COLUMNS) {
				//new line for new Row
				sb.append("\n");
				j = 0;
			}
			//Add the String representation of the contents of each cell
			sb.append(cell.toString());
			sb.append(" ");
			j++;
		}
		boardString = "\n" + sb.toString() + "\n";
		return boardString;
	}

	/**
	 * Initialises the Start Points of all Characters on the Map.
	 * Helpful for a loaded game.
	 */
	public void initialiseStartPoints() {
		//Initialise the start point for all the Characters.
		this.characterStartCells.put(Card.MISS_SCARLETT, getCell(24,7));
		this.characterStartCells.put(Card.COLONEL_MUSTARD, getCell(17,0));
		this.characterStartCells.put(Card.MRS_WHITE, getCell(0,9));
		this.characterStartCells.put(Card.MR_GREEN, getCell(0,14));
		this.characterStartCells.put(Card.MRS_PEAKCOCK, getCell(6,23));
		this.characterStartCells.put(Card.PROFESSOR_PLUM, getCell(19,23));
	}

	/**
	 * Returns a Map<Direction, Cell> of all the neighbouring Cells on the Board of the given Cell
	 * in the Cardinal directions
	 *
	 * @param cell cell to check for neighbours
	 * @return map of cells to surrounding 'cell'
	 */
	public HashMap<Direction, Cell> neighbourCells(Cell cell) {
		//extract the row and column of cell
		int currentRow = cell.getRow();
		int currentCol = cell.getCol();
		HashMap <Direction, Cell> neighbours = new HashMap<>();
		//if the row index or  column index are out of bounds, that coordinate is ignored
		if(currentRow - 1 >= 0)
			neighbours.put(Direction.UP, getCell(currentRow - 1, currentCol));
		if(currentRow + 1 < ROWS)
			neighbours.put(Direction.DOWN, getCell(currentRow + 1, currentCol));
		if(currentCol - 1 >= 0)
			neighbours.put(Direction.LEFT, getCell(currentRow, currentCol - 1));
		if(currentCol + 1 < COLUMNS)
			neighbours.put(Direction.RIGHT, getCell(currentRow, currentCol + 1));
		return neighbours;
	}


	/*
	Controls drawing of the board. Called in Main.java
	 */
	public void draw(Graphics g, int x, int y){
		HashMap<Card, CellLocationHelper> points = new HashMap<>();

		for(Card c: Card.getCardsOfType(Card.Type.ROOM)) {
			points.put(c, new CellLocationHelper());
		}

		for(Cell c: this.getCells()) {
			c.draw(g, x, y, this.neighbourCells(c));
			x += Cell.cellWidth;

			points.get(c.getRoom()).count += 1;
			points.get(c.getRoom()).x += x;
			points.get(c.getRoom()).y += y + Cell.cellHeight / 2;

			if((x / Cell.cellWidth) == Board.COLUMNS) {
				// We've reached the end of the row.
				y += Cell.cellHeight;
				x = 0;
			}
		}
		// Draw room text labels
		g.setColor(Color.BLACK);
		for(Card c : Card.getCardsOfType(Card.Type.ROOM)) {
			if(c == Card.INACCESSIBLE || c == Card.CORRIDOR) {
				continue;
			}

			CellLocationHelper location = points.get(c);

			// Draw string on centroid, minus the font size * length of string

			g.setFont(new Font(Font.SANS_SERIF,  Font.BOLD, 12));
			int textWidth = g.getFontMetrics().stringWidth(c.getDescription());
			g.drawString(c.getDescription(),
					location.averageX() - textWidth / 2 - 3,
					location.averageY());
		}
	}

}

class InvalidMoveException extends java.lang.Exception{
	public InvalidMoveException(String msg) {
		super(msg);
	}
}

/*
This is a helper function that facilitates the
identification of the geometric center of each of the rooms.
 */
class CellLocationHelper {
	public int x;
	public int y;
	public int count;

	public int averageX() {
		return x / count;
	}
	public int averageY() {
		return y / count;
	}
}