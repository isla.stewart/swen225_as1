package Model;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class GameTest {

    @org.junit.jupiter.api.Test
    void rollDice() {
        // This tests the rolling of dice and that it actually works.
        Game theModel = Game.initialiseNewGame(new String[]{"A", "B", "C", "D", "E", "F"}, new Integer[]{1, 2, 3, 4, 5, 6});

        theModel.setMovesRemaining(-1);
        int previousMovesRemaining = theModel.getMovesRemaining();
        theModel.rollDice();
        int newMovesRemaining = theModel.getMovesRemaining();

        assert previousMovesRemaining != newMovesRemaining;
    }

    /**
     * This tests that a character can move on the board
     */
    @org.junit.jupiter.api.Test
    void boardMovement() {
        Game theModel = Game.initialiseNewGame(new String[]{"A", "B", "C", "D", "E", "F"}, new Integer[]{1, 2, 3, 4, 5, 6});

        theModel.rollDice();
        Player p = theModel.getCurrentPlayer();
        Cell currentCell = p.getCurrentCell();
        Cell adjacentCell = theModel.getGameBoard().neighbourCells(currentCell).get(Board.Direction.UP);
        theModel.getGameBoard().move(theModel.getCurrentPlayer(), adjacentCell);
        assert adjacentCell.getRealPlayer() == p;
        assert !currentCell.hasRealPlayer();
    }

    /**
     * This tests that the appropriate characters are created.
     */
    @org.junit.jupiter.api.Test
    void characterCreation() {
        Game theModel = Game.initialiseNewGame(new String[]{"A", "B", "C", "D", "E", "F"}, new Integer[]{1, 2, 3, 4, 5, 6});

        assert theModel.getPlayer(0).getCharacter() == (Card.MISS_SCARLETT);
        assert theModel.getPlayer(1).getCharacter() == (Card.COLONEL_MUSTARD);

        assert theModel.getPlayer(2).getCharacter() == (Card.MRS_WHITE);
        assert theModel.getPlayer(3).getCharacter() == (Card.MR_GREEN);

        assert theModel.getPlayer(4).getCharacter() == (Card.MRS_PEAKCOCK);
        assert theModel.getPlayer(5).getCharacter() == (Card.PROFESSOR_PLUM);
    }


    /**
     * This tests that each of the rooms are somewhere on the map.
     */
    @org.junit.jupiter.api.Test
    void roomsExist() {
        Game theModel = Game.initialiseNewGame(new String[]{"A", "B", "C", "D", "E", "F"}, new Integer[]{1, 2, 3, 4, 5, 6});

        // Check each card.
        for(Card card : Card.getCardsOfType(Card.Type.ROOM)) {
            boolean cardFound = false;

            for(Cell cell : theModel.getGameBoard().getCells()) {
                if(cell.getRoom() == card) {
                    cardFound = true;
                    break;
                }
            }
            // Must be true that we can find each room card somewhere.
            assert cardFound;
        }
    }


    /**
     * This tests that a Character cannot make an invalid move on the board
     */
    @org.junit.jupiter.api.Test
    void invalidBoardMovement() {
        Game theModel = Game.initialiseNewGame(new String[]{"A", "B", "C", "D", "E", "F"}, new Integer[]{1, 2, 3, 4, 5, 6});

        theModel.rollDice();
        Player p = theModel.getCurrentPlayer();
        Cell currentCell = p.getCurrentCell();
        Cell adjacentCell = theModel.getGameBoard().neighbourCells(currentCell).get(Board.Direction.LEFT);

        theModel.getGameBoard().move(theModel.getCurrentPlayer(), adjacentCell);

        assert adjacentCell.getRealPlayer() != p;
        assert currentCell.hasRealPlayer();
    }

    /**
     * This test is to check if the suggested Character jumps to the suggested Room
     */
    @org.junit.jupiter.api.Test
    void jumpOnSuggestion() {
        Game theModel = Game.initialiseNewGame(new String[]{"A", "B", "C", "D", "E", "F"}, new Integer[]{1, 2, 3, 4, 5, 6});

        theModel.rollDice();
        Circumstance circumstance;
        if(theModel.getSolution().containsCard(Card.MISS_SCARLETT)){ // Ensure that at least one card isn't in the solution
            circumstance = new Circumstance(Card.COLONEL_MUSTARD, Card.BALLROOM, Card.CANDLESTICK);
        } else {
            circumstance = new Circumstance(Card.MISS_SCARLETT, Card.BALLROOM, Card.CANDLESTICK);
        }

        theModel.setCurrentSuggestion(circumstance);
        //checking if the suggested Character is now in the same room as the current player (or the Player making the Suggestion)
        assert theModel.getPlayers().get(Integer.parseInt(circumstance.getCharacter().toString()) - 1).getCurrentCell().getRoom() == theModel.getCurrentPlayer().getCurrentCell().getRoom();
    }


    /**
     * This tests that a suggestion can be refuted
     */
    @org.junit.jupiter.api.Test
    void refutingSuggestion() {
        Game theModel = Game.initialiseNewGame(new String[]{"A", "B", "C", "D", "E", "F"}, new Integer[]{1, 2, 3, 4, 5, 6});

        theModel.rollDice();
        Circumstance circumstance;
        if(theModel.getSolution().containsCard(Card.MISS_SCARLETT)){ // Ensure that at least one card isn't in the solution
            circumstance = new Circumstance(Card.COLONEL_MUSTARD, Card.BALLROOM, Card.CANDLESTICK);
        } else {
            circumstance = new Circumstance(Card.MISS_SCARLETT, Card.BALLROOM, Card.CANDLESTICK);
        }

        theModel.setCurrentSuggestion(circumstance);
        // Check that someone can refute
        boolean canRefute = theModel.checkForRefutation();
        Player refuter = theModel.getCurrentRefuter(); // Get that player
        // Make sure the refuter contains at least one of the cards suggested
        assert canRefute && Stream.of(Card.COLONEL_MUSTARD, Card.MISS_SCARLETT, Card.BALLROOM, Card.CANDLESTICK).anyMatch(card -> refuter.getHand().containsCard(card));

        // Check that a correct suggestion cannot be refuted
        theModel.setCurrentSuggestion(theModel.getSolution());
        canRefute = theModel.checkForRefutation();
        assert !canRefute;
    }

    /**
     * Test that a loaded game matches the game saved
     */
    @org.junit.jupiter.api.Test
    void loadingGame() {
        Game theModel = Game.initialiseNewGame(new String[]{"Alpha", "Bravo", "Charlie", "Delta", "", ""}, new Integer[]{4, 1, 5, 6, 0, 0});
        Serializer serializer = new Serializer();
        String gameString = serializer.saveGame(theModel);

        // Load From String
        Serializer loadFromString = new Serializer();
        loadFromString.generateGame(gameString);
        Game stringModel = Game.loadGame(loadFromString);
        assert theModel.compareTo(stringModel);

        // Load From File
        File gameFile = new File("gameFile");
        try {
            FileWriter fileWriter = new FileWriter(gameFile);
            fileWriter.write(gameString);
            fileWriter.close();
        } catch (IOException e) {
            fail();
        }
        Serializer loadFromFile = new Serializer();
        loadFromFile.loadFromFile(gameFile);
        Game fileModel = Game.loadGame(loadFromFile);
        assert theModel.compareTo(fileModel); // Compares the board, players, currentPlayer and solution

        // Invalid game string (too short)
        String failString = "This will fail";
        Serializer fail = new Serializer();
        assertFalse(fail.generateGame(failString));

        // Invalid game string (invalid format)
        failString = "*********_****_*********kkkkkk*___bbbb___*cccccckkkkkk__bbbbbbbb__cccccckkkkkk__bbbbbbbb__cccccckkkkkk__bbbbbbbb__cccccckkkkkk__bbbbbbbb___cccc**kkkkk__bbbbbbbb________________bbbbbbbb_______**_________________BBBBBBddddd_____________BBBBBBdddddddd__*****___BBBBBBdddddddd__*****___BBBBBBdddddddd__*****___BBBBBBdddddddd__*****________*dddddddd__*****___lllll*dddddddd__*****__lllllll*_________*****__lllllll_________________lllllll*________hhhhhh___lllll*LLLLLLL__hhhhhh_________LLLLLLL__hhhhhh________*LLLLLLL__hhhhhh__sssssssLLLLLLL__hhhhhh__sssssssLLLLLLL__hhhhhh__sssssssLLLLLL*_*hhhhhh*_*ssssss00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000010000000000001100000011010000000001000000000000000000000001000010000100000000000000000010000100000000000000000000000000110000000000000000000000000000000000000000000000000000000000001100000000000001000000000000000000000101000000000000000000000100000000010000000000000000000000010000000001100000000000000000110000000000000000010000110000000000000000010000000000000000000000000000000110100000000000000000000000100000000000000000000000000000000000000000000000000000000000000000000000000000006,6,6,1,1,0,0,32,7,M-k-d-3-1,*,Isla,2,7,0,l-h-B-2-X,*,Amer,3,1,0,0,9,,*,C,4,1,0,0,14,,*,D,5,0,0,6,23,4-Y-c-Z,*,Evan,6,0,0,19,23,W-5-s-b,*,Max";
        fail = new Serializer();
        assertFalse(fail.generateGame(failString));
    }


}