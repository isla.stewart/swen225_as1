import Controller.Controller;
import View.GUI;
import Model.Game;

public class Cluedo {
    public static void main(String[] args) {
        Game theModel = Game.initialiseNewGame(new String[]{"A", "B", "C", "D", "E", "F"}, new Integer[]{1, 2, 3, 4, 5, 6});
        GUI theView = new GUI();
        Controller theController = new Controller(theView, theModel);
    }
}
